Computer Vision Solar Tracking :sunny:

Due to the detected educational demands regarding automatic control and training in management of solar tracker systems, a small prototype based on low cost open source hardware and computer vision was built to test the control algorithms developed in Mathematica and Simulink. The prototype has been developed with the aim of being a powerful tool for a wide range of applications regarding to the learning of automatic in solar energy. This repository contains the necessary code for the control by artificial vision of the prototype.


More information:

Carballo, J. A., Bonilla, J., Roca, L,. Berenguel, M.. New low-cost solar tracking system based on open source hardware for educational purposes. Solar energy. (2018) 









